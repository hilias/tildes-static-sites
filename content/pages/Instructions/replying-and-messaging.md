Title: Replying and Messaging

[TOC]

### Notifying users

You can send a notification to another user and draw their attention to a comment, without replying to one of their topics or comments. To do so, type @username in the comment. /u/username and u/username will also work. This will send a notification to the other user's inbox. There is no limit on the number of users you can tag in a single comment.

**User notifications do not work in topics**, only in comments.

Reference: [Username mentions in comments now send notifications](https://tildes.net/~tildes.official/4i5/username_mentions_in_comments_now_send_notifications)
