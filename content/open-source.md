Title: Tildes is open-source
Date: July 27, 2018
Summary: The code behind Tildes is now open-source and available on GitLab.

**If this is the first that you've heard of Tildes, you should start by reading [the announcement blog post](https://blog.tildes.net/announcing-tildes) for general information about the project and its goals.**

---

One of the overarching themes of Tildes is to involve the community as much as possible. This starts from the very foundations: the company is a non-profit [supported by user donations](https://docs.tildes.net/donate)&mdash;there are no investors, advertisers, or other outside influences. This approach ensures that the site's priorities will stay aligned with its users', and that development can be done in an open and honest manner.

To truly support this, the Tildes code is now [open-source and available on GitLab](https://gitlab.com/tildes/tildes), with all further development being done publicly. Contributions are welcome, and several fixes and updates by other developers have already been integrated since it was open-sourced last week.

## Jumping in

The rest of this blog post is going to briefly go over some general information related to Tildes development. If you'd rather jump right into looking at the code or tinkering with a development version, please start by reading [the project's CONTRIBUTING file](https://gitlab.com/tildes/tildes/blob/master/CONTRIBUTING.md), which will point you to all the information you need.

## License

The Tildes code is licensed as [AGPLv3](https://www.gnu.org/licenses/why-affero-gpl.html). Much like organizing as a non-profit, this license choice represents a long-term commitment to principles. It ensures that the code will always remain open, and that people can contribute to it with confidence that their work can't be closed off in the future.

If any other sites (or companies) decide to use Tildes code as a base, this license will also require them to open-source their changes and additions.

## Technology

From the technical side, Tildes aims to [use modern versions of simple, reliable, "boring" technology](https://docs.tildes.net/philosophy/site-implementation#use-modern-versions-of-simple-reliable-boring-technology). This is especially important as an open-source project hoping for outside contributions, since it means that people can get involved much more easily.

The core technologies are [Python 3.7](https://docs.python.org/3.7/) (using the [Pyramid](http://docs.pylonsproject.org/projects/pyramid/en/latest/) web framework) and [PostgreSQL 10](https://www.postgresql.org/docs/10/static/). A few other systems are used for specific needs, but overall, Tildes is structured as a fairly "old-school" server-side web application, with Python and PostgreSQL handling as much as possible.

## Approachability

One of my main goals for Tildes as an open-source project is to have it be approachable even for relatively inexperienced developers and people that have no experience contributing to other open-source projects. Web applications are complex, but being able to contribute changes to a site that you use can be exciting, and is a great motivation to learn.

The Tildes code has been built with this in mind and uses various methods to improve its quality and consistency, including strict code style (using [Black](https://black.readthedocs.io/)), full Python type declarations (via [mypy](http://mypy-lang.org/)), and extensive commenting. By being disciplined about these aspects from the very beginning, the code will be able to stay far more approachable for interested contributors.

## Getting involved

If you'd like to be a part of Tildes, there are a few possibilities:

* The site has been in invite-only alpha for a couple of months now. It's fairly active already, but more users are certainly welcome. If you'd like an invite, please send an email to [invites@tildes.net](mailto:invites@tildes.net) to request one.
* If you're interested in contributing to Tildes's development, please read [the CONTRIBUTING file](https://gitlab.com/tildes/tildes/blob/master/CONTRIBUTING.md) for more links and information about how to get started.
* As always, [donations are much appreciated](https://docs.tildes.net/donate) and enable me to continue focusing on developing Tildes.

Thanks for your interest and support so far&mdash;the first few months of Tildes have gone very well, and I'm excited to see how being open-source is able to help the site progress even more quickly and openly.
