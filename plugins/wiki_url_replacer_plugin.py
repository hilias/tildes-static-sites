from urllib.parse import urlparse, urlunparse

from bs4 import BeautifulSoup

from pelican.signals import content_written


def replace_wiki_urls(path, context):
    """Replace any urls pointing to the ~tildes.official wiki with Docs ones."""
    with open(path, "r", encoding="utf-8") as f:
        soup = BeautifulSoup(f.read(), features="html5lib")

    for link in soup.find_all("a"):
        if "href" not in link.attrs:
            continue

        parsed = urlparse(link["href"])

        if parsed.netloc != "tildes.net":
            continue

        prefix = "/~tildes.official/wiki/"

        if not parsed.path.startswith(prefix):
            continue

        # remove the prefix from the path and replace underscores with hyphens
        new_path = parsed.path[len(prefix)-1:]
        new_path = new_path.replace("_", "-")

        # convert to docs.tildes.net, use modified path, and replace in the fragment too
        parsed = parsed._replace(
            scheme="https",
            netloc="docs.tildes.net",
            path=new_path,
            fragment=parsed.fragment.replace("_", "-"),
        )

        link["href"] = urlunparse(parsed)

    # write the file back out with the modified links, replacing original
    with open(path, "w", encoding="utf-8") as f:
        f.write(str(soup))


def register():
    """Register the method to run after writing each piece of content."""
    content_written.connect(replace_wiki_urls)
